PROJECT = tuplespace
PROJECT_VERSION = 1.0.0
PROJECT_DESCRIPTION = An erlang aplication providing a sharded in memory tuplespace (ets) based on consistency hashing of the tuple key

include erlang.mk

SHELL_OPTS = +P 5000000 \
 +K true \
 -pa ebin \
 -boot start_sasl \
 -config dev.config \
 -s rb \
 -s $(PROJECT) \
 -sname $(PROJECT)

clean-logs:
	@rm log/*

docker-push:
	docker build --pull=true --no-cache=true -t docker.rand.dev.williamhill.plc:5000/epocholith/$(PROJECT):$(PROJECT_VERSION) .
	docker push docker.rand.dev.williamhill.plc:5000/epocholith/$(PROJECT):$(PROJECT_VERSION)
