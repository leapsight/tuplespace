
-define(QUEUE_TABLE_NAME, queue).

-define(REGISTRY_TABLE_NAME, table_registry).

-define(QUEUE_TABLE(Term),
    tuplespace:locate_table(?QUEUE_TABLE_NAME, Term)).

-define(REGISTRY_TABLE(Term),
    tuplespace:locate_table(?REGISTRY_TABLE_NAME, Term)).

-define(TABLE_SPECS, [
    {?QUEUE_TABLE_NAME, ?TABLE_PROPS(ordered_set, 1)},
    {?REGISTRY_TABLE_NAME, ?TABLE_PROPS(set, 2)}
]).

-define(TABLE_PROPS(Type, KeyPos), [
    Type,
    {keypos, KeyPos}
    | ?DEFAULT_TABLE_PROPS
]).


-ifdef(OTP_RELEASE).
  %% OTP 21 or higher
  -if(?OTP_RELEASE >= 23).
    -define(DEFAULT_TABLE_PROPS, [
        named_table,
        public,
        {read_concurrency, true},
        {write_concurrency, true},
        {decentralized_counters, true}
    ]).
  -endif.
-else.
    -define(DEFAULT_TABLE_PROPS, [
        named_table,
        public,
        {read_concurrency, true},
        {write_concurrency, true}
    ]).
-endif.


%% @doc The table_registry record maps a name to an ets tid.
%% This entries are stored in the tuplespace REGISTRY_TABLE
-record(table_registry, {
    name            ::  any(),
    tid             ::  non_neg_integer()
}).
