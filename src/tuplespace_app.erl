-module(tuplespace_app).
-behaviour(application).
-include ("tuplespace.hrl").

-export([start/2]).
-export([stop/1]).
-export([priv_dir/0]).
-export([start_phase/3]).



start(_Type, _Args) ->
    tuplespace_sup:start_link().

%% -----------------------------------------------------------------------------
%% @doc Does nothing. Added to allow tuplespace to be included on apps using
%% phased startup.
%% @end
%% -----------------------------------------------------------------------------
start_phase(_, _, _) ->
    ok.


stop(_State) ->
    ok.



priv_dir() ->
    case code:priv_dir(tuplespace) of
        {error, bad_name} ->
            filename:join([filename:dirname(code:which(?MODULE)), "..", "priv"]);
        A ->
            A
    end.





%% =============================================================================
%% PRIVATE
%% =============================================================================

