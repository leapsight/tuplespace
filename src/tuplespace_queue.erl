%% -----------------------------------------------------------------------------
%% @doc This module implements a partition queue on a tuplespace using
%% {@link tuplespace}.
%% It provides two interfaces:
%%  * a normal FIFO queue interface with the functions {@link enqueue/2}
%% and {@link dequeue/1}
%% * a FIFO queue with expiration with the functions {@link enqueue/3}
%% and {@link dequeue_expired/1}
%% @end
%% -----------------------------------------------------------------------------
-module(tuplespace_queue).
-include("tuplespace.hrl").
-include_lib("kernel/include/logger.hrl").

-define(TABLES, tuplespace:tables(?QUEUE_TABLE_NAME)).

-export([init/0]).
-export([delete/1]).
-export([dequeue/1]).
-export([dequeue/2]).
-export([dequeue_expired/1]).
-export([dequeue_expired/2]).
-export([enqueue/2]).
-export([enqueue/3]).
-export([is_empty/1]).
-export([peek/1]).
-export([peek/2]).
-export([peek_expired/1]).
-export([peek_expired/2]).
-export([remove_expired/1]).
-export([evict_expired/0]).
-export([evict_expired/1]).
-export([remove/2]).
-export([size/1]).
-export([partitions/0]).
-export([to_list/1]).
-export([partition_evict_expired/1]).


-type enqueue_options()         ::  #{
    key => any(),
    ttl => pos_integer() | infinity,
    on_evict => exp_fun()
}.

-type dequeue_options()         ::  #{
    key => any(),
    n => pos_integer() | infinity
}.

-type exp_fun() :: fun((any()) -> any()).
-type entry() :: {Key :: any(), Value :: any(), OnExp :: exp_fun()}.

-compile({no_auto_import,[size/1]}).



%% =============================================================================
%% API
%% =============================================================================



%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
init() ->
    %% TODO THIS IS A POORSMAN solution. Once we have a sharded pool of workers
    %% that can be assigned to tables, we will have an eviction manager
    Fun = fun() ->
        ok = evict_expired(),
        %% We sleep for 30 secs (jobs standard min rate is 1/sec)
        timer:sleep(30 * 1000)
    end,
    jobs:add_queue(tuplespace_queue_eviction,[
        {producer, Fun},
        {regulators, [
            {counter, [{limit, 1}]}
        ]}
    ]).


%% -----------------------------------------------------------------------------
%% @doc
%% Returns an integer representing the number of elements found in the
%% queue with name Q.
%% @end
%% -----------------------------------------------------------------------------
-spec size(atom()) -> non_neg_integer().
size(Q) ->
    MP = {{Q, '_', '_', '_'}, '_', '_'},
    MS = [{MP, [], ['true']}],
    ets:select_count(?QUEUE_TABLE(Q), MS).


%% -----------------------------------------------------------------------------
%% @doc
%% Returns the list of partition ids a.k.a ets:tab().
%% @end
%% -----------------------------------------------------------------------------
-spec partitions() -> [ets:tab()].
partitions() ->
    ?TABLES.


%% -----------------------------------------------------------------------------
%% @doc
%% Delete queue with name Q.
%% @end
%% -----------------------------------------------------------------------------
-spec delete(atom()) -> ok.
delete(Q) ->
    MP = {{Q, '_', '_', '_'}, '_', '_'},
    true = ets:match_delete(?QUEUE_TABLE(Q), MP),
    ok.


%% -----------------------------------------------------------------------------
%% @doc
%% Tests if Q is empty and returns 'true' if so and 'false' otherwise.
%% @end
%% -----------------------------------------------------------------------------
-spec is_empty(atom()) -> boolean().
is_empty(Q) ->
    size(Q) == 0.


%% -----------------------------------------------------------------------------
%% @doc
%% Returns a list of the items in the queue in the same order; the front item
%% of the queue will become the head of the list.
%% It only returns valid items i.e. it excludes expired items.
%% @end
%% -----------------------------------------------------------------------------
-spec to_list(atom()) -> list().
to_list(Q) ->
    Tab = ?QUEUE_TABLE(Q),
    MP = {{Q, '_', '$1', '_'}, '$2', '_'},
    Conds = [{'>', '$1', {const, erlang:system_time(second)}}],
    MS = [{MP, Conds, ['$2']}],
    ets:select(Tab, MS).


%% -----------------------------------------------------------------------------
%% @doc
%% Inserts Item at the rear of queue Q. Returns the atom 'ok'.
%% Calls {@link enqueue/3} with TTLSecs equal to 'infinity'.
%% @end
%% -----------------------------------------------------------------------------
-spec enqueue(atom(), any()) -> ok.
enqueue(Q, Item) ->
    enqueue(Q, Item, #{ttl => infinity}).



%% -----------------------------------------------------------------------------
%% @doc
%% Inserts Item at the rear of queue Q, setting a time to live TTLSecs in
%% seconds. Returns the atom 'ok'.
%% @end
%% -----------------------------------------------------------------------------
-spec enqueue(atom(), any(), enqueue_options()) -> ok.
enqueue(Q, Item, Opts) ->
    TS = erlang:system_time(microsecond),
    FT = future_time(Opts),
    Key = maps:get(key, Opts, undefined),
    ExpFun = maps:get(on_evict, Opts, undefined),
    is_function(ExpFun, 1)
    orelse ExpFun =:= undefined
    orelse error({badarg, Opts}),
    ets:insert(?QUEUE_TABLE(Q), {{Q, TS, FT, Key}, Item, ExpFun}),
    ok.


%% -----------------------------------------------------------------------------
%% @doc
%% Returns a non-expired item at the front of queue Q without removing it.
%% If Q is empty, the atom 'empty' is returned.
%% If you want to inspect expired items use {@link peek_expired/1} instead.
%% This function imposes a limit of 1000 items returned at a time.
%% @end
%% -----------------------------------------------------------------------------
-spec peek(atom()) -> list() | empty.
peek(Q) ->
    peek(Q, #{n => 1}).


%% -----------------------------------------------------------------------------
%% @doc
%% Removes N items at the front of queue Q. Returns Items, where Items is
%% the list of items removed. If Q is empty, the empty list is returned.
%% The items will be removed only if their time-to-live has not been reached.
%% Fails with reason empty if Q is empty.
%% This function imposes a limit of 1000 items returned at a time.
%% @end
%% -----------------------------------------------------------------------------
-spec peek(atom(), dequeue_options()) -> list() | empty.
peek(Q, Opts) ->
    case read(Q, Opts) of
        empty ->
            empty;
        Objs ->
            [Term || {_K, Term, _ExpFun} <- Objs]
    end.


%% -----------------------------------------------------------------------------
%% @doc
%% Starting at the front of the queue Q, it returns the first expired item.
%% An expired item is one for which its time-to-live (TTLSecs) has been reached
%% but has not been evicted yet.
%% Returns the item or the atom 'empty' if there are no expired items.
%% Items that have been inserted in the queue using {@link enqueue/2} will be
%% skipped.
%% This function imposes a limit of 1000 items returned at a time.
%% @end
%% -----------------------------------------------------------------------------
-spec peek_expired(atom()) -> list() | empty.
peek_expired(Q) ->
    peek_expired(Q, #{n => 1}).


%% -----------------------------------------------------------------------------
%% @doc
%% Starting at the front of the queue Q, it returns N expired items.
%% An expired item if one for which its time-to-live (TTLSecs) has been reached
%% but has not been evicted yet.
%% If Q is empty, the empty list is returned.
%% Items that have been inserted in the queue using {@link enqueue/2} will be
%% skipped.
%% This function imposes a limit of 1000 items returned at a time.
%% @end
%% -----------------------------------------------------------------------------
-spec peek_expired(atom(), dequeue_options()) -> list() | empty.
peek_expired(Q, Opts) ->
    case read_expired(Q, Opts) of
        empty ->
            empty;
        Objs ->
            [Term || {_K, Term, _ExpFun} <- Objs]
    end.


%% -----------------------------------------------------------------------------
%% @doc
%% Removes the item at the front of queue Q. Returns Item, where Item is
%% the item removed. If Q is empty, the atom 'empty' is returned.
%% The item will be removed regardless of its time-to-live. If you want to
%% respect the time-to-live use {@link dequeue_expired/1} instead.
%% This function imposes a limit of 1000 items returned at a time.
%% @end
%% -----------------------------------------------------------------------------
-spec dequeue(atom()) -> list() | empty.
dequeue(Q) ->
    dequeue(Q, #{n => 1}).


%% -----------------------------------------------------------------------------
%% @doc
%% Removes N items at the front of queue Q. Returns Items, where Items is
%% the list of items removed. If Q is empty, the empty list is returned.
%% The items will be removed only if their time-to-live has not been reached.
%% This function imposes a limit of 1000 items returned at a time.
%% @end
%% -----------------------------------------------------------------------------
-spec dequeue(atom(), dequeue_options()) -> list() | empty.
dequeue(Q, Opts) ->
    Tab = ?QUEUE_TABLE(Q),
    case read(Q, Opts) of
        empty ->
            empty;
        Objs ->
            Fun = fun({K, _, _}, Acc) ->
                %% This is an ordered set so take always returns
                %% at most 1 element
                case ets:take(Tab, K) of
                    [] ->
                        Acc;
                    [{K, Removed, _}] ->
                        [Removed|Acc]
                end
            end,
            maybe_empty(lists:foldl(Fun, [], Objs))
    end.



%% -----------------------------------------------------------------------------
%% @doc
%% Starting at the front of the queue Q, it removes the first expired item.
%% An expired item is one for which its time-to-live (TTLSecs) has been reached.
%% Returns the removed item or the atom 'empty' if there are no expired items.
%% Items that have been inserted in the queue using {@link enqueue/2} will be
%% skipped.
%% Fails with reason empty if Q is empty.
%% This function imposes a limit of 1000 items returned at a time.
%% @end
%% -----------------------------------------------------------------------------
-spec dequeue_expired(atom()) -> list() | empty.
dequeue_expired(Q) ->
    dequeue_expired(Q, #{n => 1}).


%% -----------------------------------------------------------------------------
%% @doc
%% Starting at the front of the queue Q, it removes N expired items.
%% Returns Items, where Items is the list of items removed.
%% An expired item if one for which its time-to-live (TTLSecs) has been reached.
%% If Q is empty, the empty list is returned.
%% Items that have been inserted in the queue using {@link enqueue/2} will be
%% skipped.
%% This function imposes a limit of 1000 items returned at a time.
%% @end
%% -----------------------------------------------------------------------------
-spec dequeue_expired(atom(), dequeue_options()) -> list() | empty.
dequeue_expired(Q, Opts) ->
    Tab = ?QUEUE_TABLE(Q),
    case read_expired(Q, Opts) of
        empty ->
            empty;
        Objs ->
            Fun = fun({K, _, _}, Acc) ->
                %% This is an ordered set so take always returns
                %% at most 1 element
                [{K, Removed, _}] = ets:take(Tab, K),
                [Removed|Acc]
            end,
            maybe_empty(lists:foldl(Fun, [], Objs))
    end.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec evict_expired() -> ok.
evict_expired() ->
    _ = [do_evict_expired(Tab) || Tab <- ?TABLES],
    ok.


%% -----------------------------------------------------------------------------
%% @doc
%% Starting at the front of the queue Q, it removes all expired items.
%% If the item was enqueued using the option `on_expiry`, the
%% the bound function will be called passing the enqueued item as argument.
%% An expired item if one for which its time-to-live (TTLSecs) has been reached.
%% Returns the atom 'ok'.
%% @end
%% -----------------------------------------------------------------------------
-spec evict_expired(atom()) -> ok.
evict_expired(Q) ->
    do_evict_expired(?QUEUE_TABLE(Q)).


%% -----------------------------------------------------------------------------
%% @doc
%% Starting at the front of the partition (table) Tab, removes
%% all expired items.
%% If the item was enqueued using the option `on_expiry`, the
%% the bound function will be called passing the enqueued item as argument.
%% An expired item if one for which its time-to-live (TTLSecs) has been reached.
%% Returns the atom 'ok'.
%% @end
%% -----------------------------------------------------------------------------
-spec partition_evict_expired(ets:tab()) -> ok.

partition_evict_expired(Tab) ->
    do_evict_expired(Tab).


%% -----------------------------------------------------------------------------
%% @doc
%% Removes all items matching key. If the key '_' is provided it removes all
%% the elements in the queue (including expired elements).
%% Returns the number of items removed.
%% The items will be removed regardless of their TTL.
%% @end
%% -----------------------------------------------------------------------------
-spec remove(atom(), any() | '_') -> non_neg_integer().
remove(Q, Key) ->
    MP = {{Q, '_', '$1', Key}, '_', '_'},
    MS = [{MP, [], ['true']}],
    ets:select_delete(?QUEUE_TABLE(Q), MS).



%% -----------------------------------------------------------------------------
%% @doc
%% Starting at the front of the queue Q, it removes N expired items.
%% Returns the number of items removed.
%% An expired item if one for which its time-to-live (TTLSecs) has been reached.
%% If Q is empty, the empty list is returned.
%% Items that have been inserted in the queue using {@link enqueue/2} will be
%% skipped.
%% @end
%% -----------------------------------------------------------------------------
-spec remove_expired(atom()) -> non_neg_integer().
remove_expired(Q) ->
    MP = {{Q, '_', '$1', '_'}, '_', '_'},
    Conds = [{'=<', '$1', {const, erlang:system_time(second)}}],
    MS = [{MP, Conds, ['true']}],
    ets:select_delete(?QUEUE_TABLE(Q), MS).



%% =============================================================================
%% PRIVATE
%% =============================================================================


%% @private
-spec read(atom(), dequeue_options()) -> list(entry()) | empty.
read(Q, Opts) ->
    Tab = ?QUEUE_TABLE(Q),
    Key = maps:get(key, Opts, '_'),
    N = min(maps:get(n, Opts, 1), 1000),
    MP = {{Q, '_', '$1', Key}, '_', '_'},
    Conds = [{'>', '$1', {const, erlang:system_time(second)}}],
    MS = [{MP, Conds, ['$_']}],
    case ets:select(Tab, MS, N) of
        '$end_of_table' ->
            empty;
        {Objs, _Cont} ->
            Objs
    end.


%% @private
-spec read_expired(atom(), dequeue_options()) -> list(entry()) | empty.
read_expired(Q, Opts) ->
    Tab = ?QUEUE_TABLE(Q),
    Key = maps:get(key, Opts, '_'),
    N = min(maps:get(n, Opts, 1), 1000),
    MP = {{Q, '_', '$1', Key}, '_', '_'},
    Conds = [{'=<', '$1', {const, erlang:system_time(second)}}],
    MS = [{MP, Conds, ['$_']}],
    case ets:select(Tab, MS, N) of
        '$end_of_table' ->
            empty;
        {Objs, _Cont} ->
            Objs
    end.

do_evict_expired(Tab) ->
    N = 100,
    %% We do a tables scan reading all logical queues
    MP = {{'_', '_', '$1', '_'}, '_', '_'},
    Conds = [{'=<', '$1', {const, erlang:system_time(second)}}],
    MS = [{MP, Conds, ['$_']}],
    case ets:select(Tab, MS, N) of
        '$end_of_table' ->
            ok;
        {Objs, _Cont} ->
            _ = [
                begin
                    %% This is an ordered_set so take always returns
                    %% at most 1 element
                    case ets:take(Tab, K) of
                        [] ->
                            %% The item was removed by some other process
                            %% since we read it, do nothing
                            ok;
                        [O] ->
                            maybe_apply(Fun, E)
                    end
                end || {K, E, Fun} = O <- Objs],
            %% Continue evicting remaining elements
            do_evict_expired(Tab)
    end.



%% @private
future_time(#{ttl := TTLSecs}) when is_integer(TTLSecs), TTLSecs > 0 ->
    erlang:system_time(second) + TTLSecs;
future_time(#{ttl := infinity}) ->
    infinity;
future_time(#{ttl := Val}) ->
    error({badarg, Val});
future_time(#{}) ->
    infinity.

%% @private
maybe_empty([]) -> empty;
maybe_empty(L) -> L.


%% @private
maybe_apply(undefined, _) ->
    ok;

maybe_apply(Fun, V) ->
    try
        _ = Fun(V),
        ok
    catch
        Class:Reason:Stacktrace ->
            ?LOG_ERROR(#{
                message => <<"Error while applying on_evict function">>,
                class => Class,
                reason => Reason,
                stacktrace => Stacktrace
            }),
            ok
    end.
